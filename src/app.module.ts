import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
// Router Module
import { RouterModule } from '@nestjs/core';
import { CaslModule } from './Casl/casl.module';
import { ThrottlerModule, ThrottlerGuard } from '@nestjs/throttler';
import { APP_GUARD } from '@nestjs/core';
// static
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
// modules
import { HomePageModule } from './router/HomePage/home.module';
import { AuthModule } from './Auth/auth.module';
import { UserModule } from './router/User/user.module';
import { CategoryModule } from './router/Category/category.module';
import { LandingPageModule } from './router/LangdingPage/landingPage.module';
import { DoaminModule } from './router/domain/domain.module';
import { BillModule } from './router/Bill/bill.module';
import { ProductModule } from './router/Product/product.module';
import { StaffModule } from './router/Staff/staff.module';
import { CustomerModule } from './router/Customer/customer.module';
import { OrderDetailModule } from './router/orderDetail/orderDetail.module';
import { OrderModule } from './router/order/order.module';
import { UploadModule } from './Upload/upload.module';
import { MulterModule } from '@nestjs/platform-express';
import { BrandModule } from './router/brand/brand.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    StaffModule,
    AuthModule,
    UserModule,
    CategoryModule,
    LandingPageModule,
    DoaminModule,
    CaslModule,
    BillModule,
    ProductModule,
    BrandModule,
    CustomerModule,
    OrderModule,
    OrderDetailModule,
    UploadModule,

    ThrottlerModule.forRoot([
      {
        ttl: 60000,
        limit: 100,
      },
    ]),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'client'),
      exclude: ['/public/(.*)'],
    }),
    RouterModule.register([
      {
        path: 'api',
        children: [
          { path: 'auth', module: AuthModule },
          { path: 'staff', module: StaffModule },
          { path: 'user', module: UserModule },
          { path: 'bill', module: BillModule },
          { path: 'domain', module: DoaminModule },
          { path: 'category', module: CategoryModule },
          { path: 'brand', module: BrandModule },
          { path: 'product', module: ProductModule },
          { path: 'customer', module: CustomerModule },
          { path: 'order', module: OrderModule },
          { path: 'order-detail', module: OrderDetailModule },
          { path: 'landing-page', module: LandingPageModule },
          { path: 'upload', module: UploadModule },
        ],
      },
    ]),
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
  ],
})
export class AppModule {}
