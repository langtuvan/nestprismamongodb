import {
  Controller,
  Get,
  UseGuards,
  NotFoundException,
  Res,
  Req,
  Param,
  Body,
  Post,
  Put,
  Delete,
  Query,
  UploadedFile,
  UseInterceptors,
  ParseFilePipe,
  MaxFileSizeValidator,
  FileTypeValidator,
  ParseFilePipeBuilder,
  HttpStatus,
} from '@nestjs/common';
import { Response, Express } from 'express';
// auth
import { AuthGuard } from 'src/guard/auth.guard';
import { requiredRoles } from 'src/decorators/roles.decorator';
import { Role } from 'src/entries/role.enum';
// validate Dto
import { CreateCategoryDto, UpdateCategoryDto } from 'src/dto/category/';
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
// get decorateor
import { CurrentDomain } from 'src/decorators/domain.decorator';
import { CurrentUser } from 'src/decorators/user.decorator';
// service
import { UploadService } from './upload.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { readdirSync } from 'fs-extra';
//import path from 'path';
const path = require('path');

@UseGuards(AuthGuard)
@Controller()
export class UploadController {
  constructor(private readonly uploadService: UploadService) {}

  @Post('/image/654b3060e4abcf8683ed5752')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './client/uploads/654b3060e4abcf8683ed5752/image/product/',
        filename: (req: any, file: Express.Multer.File, cb: any) => {
          const filename: string = path
            .parse(file.originalname)
            .name.replace(/\s/g, '');
          const extension: string = path.parse(file.originalname).ext;
          cb(null, `${filename}${extension}`);
        },
      }),
    }),
  )
  async handleUploadImage(
    @Res() res: Response,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({ maxSize: 1200000 }),
          new FileTypeValidator({ fileType: /(jpg|jpeg|png|gif)$/ }),
        ],
      }),
    )
    file: Express.Multer.File,
  ) {
    return res.status(200).json({ imagePath: file.filename });
  }

  @Get('/image/')
  async getImages(@CurrentUser('domainId') domainId: string) {
    return readdirSync(`./client/uploads/${domainId}/image/product/`);
  }
}

// export const storage654b3060e4abcf8683ed5752 = diskStorage({
//   destination: './client/uploads/image/product/654b3060e4abcf8683ed5752/',
//   filename: (req: any, file: Express.Multer.File, cb: any) => {
//     const filename: string = path
//       .parse(file.originalname)
//       .name.replace(/\s/g, '');
//     const extension: string = path.parse(file.originalname).ext;
//     cb(null, `${filename}${extension}`);
//   },
// });
