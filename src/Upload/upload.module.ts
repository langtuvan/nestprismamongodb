import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';

import { CaslModule } from 'src/Casl/casl.module';

import { UploadService } from './upload.service';
import { UploadController } from './upload.controller';

@Module({
  imports: [CaslModule],
  controllers: [UploadController],
  providers: [PrismaService, UploadService],
})
export class UploadModule {}
