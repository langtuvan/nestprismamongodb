//import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsBoolean,
  IsDate,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export default class DomainDto {
  @IsString()
  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(50)
  name: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(50)
  @IsOptional()
  status: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(23)
  @MaxLength(24)
  plan: string;

  @IsDate()
  @MinLength(5)
  @MaxLength(50)
  @IsOptional()
  activeDate: string;

  @IsDate()
  @IsOptional()
  @MinLength(9)
  @MaxLength(11)
  expDate: string;
}
