//import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import DomainDto from './domain.dto';
import { IntersectionType } from '@nestjs/mapped-types';
import { CategoryDto } from '../category';
import { CreateUserDto, UserDto } from '../user';

export default class CreateDomainDto extends DomainDto {
  @IsOptional()
  user: CreateUserDto;

  @IsOptional()
  categories: CategoryDto[];
}
