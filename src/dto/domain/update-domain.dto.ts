import DomainDto from './domain.dto';
import { PartialType } from '@nestjs/mapped-types';

export default class UpdateDomainDto extends PartialType(DomainDto) {}
