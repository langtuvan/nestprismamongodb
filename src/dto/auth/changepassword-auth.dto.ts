//import { ApiProperty } from '@nestjs/swagger';
import { PickType } from '@nestjs/mapped-types';
import { IsNotEmpty, IsString, MaxLength, MinLength, ValidateIf } from 'class-validator';
import { PasswordAuthDto } from '../user/user.dto';

export default class ChangePasswordAuthDto extends PickType(PasswordAuthDto, [
  'oldPassword',
  'newPassword',
  'confirmPassword',
]) {

  //@ValidateIf(o => o.newPassword === o.confirmPassword)
}
