//import { ApiProperty } from '@nestjs/swagger';
import { PickType } from '@nestjs/mapped-types';
import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import UserDto from '../user/user.dto';

export default class SignUpAuthDto extends PickType(UserDto, [
  'email',
  'fullname',
  'phone',
  'password',
]) {
  @IsString()
  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(20)
  confirmPassword: string;
}
