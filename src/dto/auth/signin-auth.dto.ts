import UserDto from '../user/user.dto';
import { PickType } from '@nestjs/mapped-types';

export default class SignInAuthDto extends PickType(UserDto, [
  'email',
  'password',
]) {}
