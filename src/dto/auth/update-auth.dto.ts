import UserDto from '../user/user.dto';
import { PartialType } from '@nestjs/mapped-types';

export default class UpdateCurrentUserDto extends PartialType(UserDto) {}
