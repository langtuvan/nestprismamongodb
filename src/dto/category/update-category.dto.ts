//import { ApiProperty } from '@nestjs/swagger';
import {} from 'class-validator';
import CategoryDto from './category.dto';
import { PartialType } from '@nestjs/mapped-types';

export default class UpdateCategoryDto extends PartialType(CategoryDto) {}
