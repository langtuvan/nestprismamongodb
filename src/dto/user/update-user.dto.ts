import UserDto from './user.dto';
import { PartialType } from '@nestjs/mapped-types';

export default class UpdateUserDto extends PartialType(UserDto) {}
