//import { ApiProperty } from '@nestjs/swagger';
import {} from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';
import ProductDto from './product.dto';

export default class UpdateProductDto extends PartialType(ProductDto) {}
