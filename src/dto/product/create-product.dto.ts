//import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId, IsNotEmpty } from 'class-validator';
import ProductDto from './product.dto';

export default class CreateProductDto extends ProductDto {}
