//import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
  isArray,
} from 'class-validator';

export default class ProductDto {
  //@IsMongoId()
  @IsNotEmpty()
  catId: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(2)
  @MaxLength(200)
  name: string;

  @IsString()
  @IsOptional()
  @MinLength(2)
  @MaxLength(200)
  code: string;

  // @IsString()
  // @IsOptional()
  // @MinLength(2)
  // @MaxLength(50)
  // barCode: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(2)
  @MaxLength(200)
  fullName: string;

  @IsBoolean()
  @IsOptional()
  allowsSale: boolean;

  @IsBoolean()
  @IsOptional()
  canReserve: boolean;

  // @IsBoolean()
  // @IsOptional()
  // hasVariants: boolean;

  // @IsBoolean()
  // @IsOptional()
  // isProductSerial: boolean;

  @IsString()
  @IsNotEmpty()
  @MinLength(2)
  description: string;

  @IsString()
  @IsOptional()
  @MinLength(2)
  content: string;

  // @IsOptional()
  // attributes: AttributeDto[];

  // @IsOptional()
  // features: FeatureDto[];

  // @IsString()
  // @IsNotEmpty()
  // @MinLength(2)
  // @MaxLength(50)
  // unit: string;

  // @IsOptional()
  // @IsMongoId()
  // masterProductId: string;

  // @IsOptional()
  // @IsMongoId()
  // masterUnitId: string;

  // @IsNumber()
  // @IsOptional()
  // conversionValue: number;

  @IsString()
  @IsNotEmpty()
  @MinLength(2)
  brandId: string;

  @IsNumber()
  @IsNotEmpty()
  basePrice: number;

  @IsNumber()
  @IsOptional()
  ReservePrice: number;

  @IsString()
  @IsNotEmpty()
  madeIn: string;

  @IsString()
  @IsOptional()
  importForm: string;

  @IsNumber()
  @IsOptional()
  weigh: number;

  @IsNumber()
  @IsNotEmpty()
  rating: number;

  @IsString()
  @IsNotEmpty()
  @MinLength(2)
  image: string;

  // @IsOptional()
  // images: string[];

  @IsString()
  @IsNotEmpty()
  @MinLength(2)
  slug: string;

  @IsBoolean()
  @IsOptional()
  isActive: boolean;
}

// export class AttributeDto {
//   @IsString()
//   @IsNotEmpty()
//   @MinLength(2)
//   @MaxLength(50)
//   attributeName: string;

//   @IsString()
//   @IsNotEmpty()
//   @MinLength(2)
//   @MaxLength(50)
//   attributeValue: string;
// }

// export class FeatureDto {
//   @IsString()
//   @IsNotEmpty()
//   @MinLength(2)
//   @MaxLength(50)
//   featureName: string;

//   @IsString()
//   @IsNotEmpty()
//   @MinLength(2)
//   @MaxLength(50)
//   featureValue: string;
// }
