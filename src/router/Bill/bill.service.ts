import { NotFoundException, Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { bill as Bill, Prisma } from '@prisma/client';
import * as bcrypt from 'bcrypt';

const saltRounds = 10;

@Injectable()
export class BillService {
  constructor(private prisma: PrismaService) {}

  async bills(params: {
    where?: Prisma.billWhereInput;
    select?: Prisma.billSelect;
    skip?: number;
    take?: number;
    cursor?: Prisma.billWhereUniqueInput;
    orderBy?: Prisma.billOrderByWithRelationInput;
  }) {
    const { where, select, skip, take, cursor, orderBy } = params;
    return this.prisma.bill.findMany({
      where,
      select,
      skip,
      take,
      cursor,
      orderBy,
    });
  }

  async bill(params: {
    where: Prisma.billWhereUniqueInput;
    select?: Prisma.billSelect;
  }) {
    const { where, select } = params;
    const bill = await this.prisma.bill.findUnique({
      where,
      select,
    });

    if (!bill) {
      throw new NotFoundException();
    }
    return bill;
  }

  async update(params: {
    where: Prisma.billWhereUniqueInput;
    data: Prisma.billUpdateInput;
  }) {
    const { where, data } = params;
    return this.prisma.bill.update({
      where,
      data,
    });
  }
}
