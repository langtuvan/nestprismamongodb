import {
  Controller,
  Get,
  UseGuards,
  Param,
  Post,
  Put,
  Body,
  Res,
  BadRequestException,
  Query,
  NotFoundException,
  ParseBoolPipe,
} from '@nestjs/common';
import { Response } from 'express';
// auth // guard
import { AuthGuard } from 'src/guard/auth.guard';
import { StaffRolesGuard } from 'src/guard/staffRole.guard';
import { staffRequiredRoles } from 'src/decorators/roles.decorator';
import { StaffRole } from 'src/entries/role.enum';
import { BillService } from './bill.service';
// validate Dto
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
import { CreateDomainDto, UpdateDomainDto } from 'src/dto/domain';
import BillDto from 'src/dto/bill/bill.dto';
// get decorateor
import { CurrentDomain } from 'src/decorators/domain.decorator';
import { CurrentUser } from 'src/decorators/user.decorator';
// service
import { DomainService } from '../domain/domain.service';
// type
import { addYears } from 'date-fns';


@UseGuards(AuthGuard, StaffRolesGuard)
@staffRequiredRoles(StaffRole.ACCOUNTANT)
@Controller()
export class BillController {
  constructor(
    private readonly billService: BillService,
    private readonly domainService: DomainService,
  ) {}

  @Get('/')
  async getAll(@Query('isActive', ParseBoolPipe) isActive: boolean = false) {
    return this.billService.bills({
      where: {
        isActive,
      },
    });
  }

  @Get('/:id')
  async getOne(@Param('id', CheckObjectIdPipe) id: string) {
    return this.billService.bill({
      where: { id },
    });
  }

  @Put('/payment/:id')
  async payBill(
    @Param('id', CheckObjectIdPipe) id: string,
    @Body() data: BillDto,
    @CurrentUser('id') CurrentUser: string,
    @Res() res: Response,
  ) {
    const { payCode, amount } = data;

    const find = await this.billService.bill({
      where: { id },
    });

    if (find.isActive) {
      throw new BadRequestException('isActive is true');
    }

    const payment = await this.billService.update({
      where: { id },
      data: {
        payCode,
        amount,
        isActive: true,
        activeDate: new Date(),
        activeBy: { connect: { id: CurrentUser } },
      },
    });

    if (!payment) {
      throw new BadRequestException('active payment failed');
    }

    const activePlan = await this.domainService.update({
      where: { id: payment.domainId },
      data: {
        status: 'active',
        plan: { connect: { id: payment.planId } },
        activeDate: new Date(),
        expDate: addYears(new Date(), 1),
      },
    });

    if (!activePlan) {
      throw new BadRequestException('active domain failed');
    }

    return res.status(200).json({ payment, activePlan });
  }
}
