import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { BillController } from './bill.controller';
import { BillService } from './bill.service';
import { CaslModule } from 'src/Casl/casl.module';
import { DomainService } from '../domain/domain.service';

@Module({
  imports: [CaslModule],
  controllers: [BillController],
  providers: [PrismaService, BillService, DomainService],
})
export class BillModule {}