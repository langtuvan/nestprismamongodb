import {
  Controller,
  Get,
  UseGuards,
  Param,
  Post,
  Body,
  Delete,
  Put,
} from '@nestjs/common';

import { LandingPageService } from './landingPage.service';
import { CurrentUser } from 'src/decorators/user.decorator';
import { AuthGuard } from 'src/guard/auth.guard';
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
import { requiredRoles } from 'src/decorators/roles.decorator';
import { Role } from 'src/entries/role.enum';
import { CurrentDomain } from 'src/decorators/domain.decorator';
import { DomainService } from '../domain/domain.service';
import { RolesGuard } from 'src/guard/role.guard';

@Controller()
export class LandingPageController {
  constructor(
    private readonly landingPageService: LandingPageService,
    private readonly domainService: DomainService,
  ) {}

  // @UseGuards(AuthGuard)
  // @requiredRoles(Role.USER)
  @Get('/')
  async getAll(@CurrentUser('domainId') domainId: string) {
    return this.landingPageService.getAll({
      where: { id: domainId },
    });
  }

  @Get('/public')
  async public(@CurrentDomain() domainId: string) {
    return this.landingPageService.getAll({
      where: { id: domainId },
    });
  }

  @UseGuards(AuthGuard, RolesGuard)
  @Get('/get-domain')
  async billing(@CurrentUser('domainId') domainId: string) {
    return this.domainService.domain({
      where: { id: domainId },
      include: {
        bill: {
          select: {
            amount: true,
            activeDate: true,
            isActive: true,
            expDate: true,
            plan: true,
          },
        },
      },
    });
  }

  @Get('/:id')
  async get(
    @CurrentUser('domainId') domainId: string,
    @Param('id', CheckObjectIdPipe) id: string,
  ) {
    return this.landingPageService.get({ where: { id, domainId } });
  }

  @UseGuards(AuthGuard, RolesGuard)
  @requiredRoles(Role.USER)
  @Post('/')
  async create(@CurrentUser('domainId') domainId: string, @Body() body: any) {
    const { name, type, content, href, displayName } = body;
    return this.landingPageService.create({
      data: {
        href,
        name,
        displayName,
        type,
        content,
        domain: { connect: { id: domainId } },
      },
    });
  }

  @UseGuards(AuthGuard, RolesGuard)
  @requiredRoles(Role.USER)
  @Put('/:id')
  async update(
    @CurrentUser('domainId') domainId: string,
    @Param('id', CheckObjectIdPipe) id: string,
    @Body() body: any,
  ) {
    const { name, type, content } = body;
    return this.landingPageService.update({
      where: { id, domainId },
      data: {
        name,
        type,
        content,
      },
    });
  }

  @UseGuards(AuthGuard, RolesGuard)
  @requiredRoles(Role.USER)
  @Delete('/:id')
  async delete(
    @CurrentUser('domainId') domainId: string,
    @Param('id', CheckObjectIdPipe) id: string,
  ) {
    return this.landingPageService.delete({
      where: {
        id,
        domainId,
      },
    });
  }
}
