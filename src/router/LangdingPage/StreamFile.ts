import {
  Controller,
  Get,
  UseGuards,
  Param,
  StreamableFile,
  Post,
  Body,
  Res,
  BadRequestException,
} from '@nestjs/common';
import { Response } from 'express';
import { LandingPageService } from './landingPage.service';

import { createReadStream, createWriteStream } from 'fs';
import { readJSONSync, readFileSync } from 'fs-extra';
import { join } from 'path';
import { CurrentUser } from 'src/decorators/user.decorator';
import { AuthGuard } from 'src/guard/auth.guard';

@Controller()
export class LandingPageController {
  constructor(private readonly landingPageService: LandingPageService) {}

  @Get('/:domain/:name')
  getFile(
    @Param('domain') domain: string,
    @Param('name') name: string,
  ): StreamableFile {
    const readFile = createReadStream(
      join(process.cwd(), `./public/landing-page/${domain}/${name}.json`),
    );

    return new StreamableFile(readFile);
  }

  @Get('/pricing')
  getPricing(): StreamableFile {
    const readFile = createReadStream(
      join(process.cwd(), `./public/pricing/pricing.json`),
    );
  
    return new StreamableFile(readFile);
  }


  // @Get('/:domain/:name')
  // getFile(
  //   @Param('domain') domain: string,
  //   @Param('name') name: string,
  //   @Res() res: Response,
  // ) {
  //   const file = readJSONSync(
  //     join(process.cwd(), `./public/landingpage/${domain}/${name}.json`),
  //   );

  //   if (!file) {
  //     throw new BadRequestException('file not exits');
  //   }
  //   return res.status(200).json(file);
  // }

  @Post('/:domain/:name')
  @UseGuards(AuthGuard)
  updateFile(
    @Param('domain') domain: string,
    @Param('name') name: string,
    @Body() body: any,
    @CurrentUser('id') currentUserId: string,
    @Res() res: Response,
  ) {
    const auth = readJSONSync(
      join(process.cwd(), `./public/landing-page/${domain}/auth.json`),
    );

    if (auth.id !== currentUserId) {
      throw new BadRequestException('current session not match');
    }

    const getfile = createWriteStream(
      join(process.cwd(), `./public/landing-page/${domain}/${name}.json`),
      {},
    );

    const write = getfile.write(JSON.stringify(body));

    if (!write) {
      throw new BadRequestException('update file failed');
    }

    return res.status(200).json(true);
  }
}
