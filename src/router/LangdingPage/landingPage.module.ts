import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { LandingPageController } from './landingPage.controller';
import { LandingPageService } from './landingPage.service';
import { CaslModule } from 'src/Casl/casl.module';
import { DomainService } from '../domain/domain.service';

@Module({
  imports: [CaslModule],
  controllers: [LandingPageController],
  providers: [PrismaService, LandingPageService, DomainService],
})
export class LandingPageModule {}
