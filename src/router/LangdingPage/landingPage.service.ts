import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { landingPage as LandingPage, Prisma } from '@prisma/client';
import fs from 'fs-extra';

@Injectable()
export class LandingPageService {
  constructor(private prisma: PrismaService) {}

  async getAll(params: {
    where: Prisma.domainWhereUniqueInput;
  }): Promise<LandingPage[]> {
    const { where } = params;
    return this.prisma.domain.findUniqueOrThrow({ where }).landingPages();
  }

  async get(params: {
    where: Prisma.landingPageWhereUniqueInput;
    select?: Prisma.landingPageSelect;
  }): Promise<Partial<LandingPage>> {
    const { where, select } = params;
    return this.prisma.landingPage.findUniqueOrThrow({
      where,
      select,
    });
  }

  // mutation
  async create(params: {
    data: Prisma.landingPageCreateInput;
    select?: Prisma.landingPageSelect;
  }): Promise<LandingPage> {
    const { data, select } = params;
    return this.prisma.landingPage.create({
      data,
      select,
    });
  }

  async update(params: {
    where: Prisma.landingPageWhereUniqueInput;
    data: Prisma.landingPageUpdateInput;
    select?: Prisma.landingPageSelect;
  }): Promise<Partial<LandingPage>> {
    const { where, data, select } = params;
    return this.prisma.landingPage.update({
      where,
      data,
      select,
    });
  }

  async delete(params: {
    where: Prisma.landingPageWhereUniqueInput;
    select?: Prisma.landingPageSelect;
  }): Promise<LandingPage> {
    const { where, select } = params;
    return this.prisma.landingPage.delete({
      where,
      select,
    });
  }

  // async readJson(name: string) {
  //   try {
  //     return await fs.readJsonSync('./data/vanlt/heroes.json');
  //   } catch (error) {}
  // }

  // async writeJson(name: string, formData: any): Promise<any> {
  //   try {
  //     fs.writeJsonSync(name, formData || {});
  //     return true;
  //   } catch (e) {
  //     return { message: 'Failed to update' };
  //   }
  // }
}
