import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { OrderDetailController } from './orderDetail.controller';
import { OrderDetailService } from './orderDetail.service';
import { CaslModule } from 'src/Casl/casl.module';
import { DomainService } from '../domain/domain.service';
import { CategoryService } from '../Category/category.service';

@Module({
  imports: [CaslModule],
  controllers: [OrderDetailController],
  providers: [PrismaService, OrderDetailService],
})
export class OrderDetailModule {}
