import {
  Controller,
  Get,
  UseGuards,
  NotFoundException,
  Res,
  Param,
  Body,
  Post,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
import { Response } from 'express';
// auth
import { AuthGuard } from 'src/guard/auth.guard';
import { requiredRoles } from 'src/decorators/roles.decorator';
import { Role } from 'src/entries/role.enum';
// validate Dto
import { CreateCategoryDto, UpdateCategoryDto } from 'src/dto/category/';
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
// get decorateor
import { CurrentDomain } from 'src/decorators/domain.decorator';
import { CurrentUser } from 'src/decorators/user.decorator';
// service
import { OrderDetailService } from './orderDetail.service';

import { orderDetail as OrderDetailModel } from '@prisma/client';

// @UseGuards(AuthGuard)
// @requiredRoles(Role.USER)
@Controller()
export class OrderDetailController {
  constructor(private readonly orderDetailService: OrderDetailService) {}

  @UseGuards(AuthGuard)
  @requiredRoles(Role.USER)
  @Get('/')
  async getAll(@CurrentUser('domainId') domainId: string) {
    return this.orderDetailService.OrderDetails({
      where: { id: domainId },
    });
  }

  @Get('/:id')
  async get(
    @CurrentUser('domainId') domainId: string,
    @Param('id', CheckObjectIdPipe) id: string,
  ) {
    return this.orderDetailService.orderDetail({ where: { id, domainId } });
  }

  @UseGuards(AuthGuard)
  @requiredRoles(Role.USER)
  @Post('/')
  async create(
    @CurrentUser('domainId') domainId: string,
    @Query('orderId', CheckObjectIdPipe) orderId: string,
    @CurrentUser('id') updatedBy: string,
    @Body() body: any,
  ) {
    const { productId, price, qty } = body;
    return this.orderDetailService.create({
      data: {
        price,
        qty,
        product: { connect: { id: productId } },
        order: { connect: { id: orderId } },
        domain: { connect: { id: domainId } },
        updatedBy: { connect: { id: updatedBy } },
      },
    });
  }

  @UseGuards(AuthGuard)
  @requiredRoles(Role.USER)
  @Put('/:id')
  async update(
    @CurrentUser('domainId') domainId: string,
    @CurrentUser('id') updatedBy: string,
    @Param('id', CheckObjectIdPipe) id: string,
    @Body() body: any,
  ) {
    const { qty, price, status } = body;
    return this.orderDetailService.update({
      where: { id, domainId },
      data: {
        qty,
        price,
        status,
        updatedBy: { connect: { id: updatedBy } },
      },
    });
  }

  @UseGuards(AuthGuard)
  @requiredRoles(Role.USER)
  @Delete('/:id')
  async delete(
    @CurrentUser('domainId') domainId: string,
    @Param('id', CheckObjectIdPipe) id: string,
  ) {
    return this.orderDetailService.delete({
      where: {
        id,
        domainId,
      },
    });
  }
}
