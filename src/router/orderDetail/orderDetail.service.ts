import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { orderDetail as OrderDetail, Prisma } from '@prisma/client';

@Injectable()
export class OrderDetailService {
  constructor(private prisma: PrismaService) {}

  async OrderDetails(params: {
    where: Prisma.domainWhereUniqueInput;
  }): Promise<OrderDetail[]> {
    const { where } = params;
    return  this.prisma.domain.findUniqueOrThrow({ where }).orderDetails();
  }

  async orderDetail(params: {
    where: Prisma.orderDetailWhereUniqueInput;
    select?: Prisma.orderDetailSelect;
  }): Promise<Partial<OrderDetail> | null> {
    const { where, select } = params;
    return this.prisma.orderDetail.findUniqueOrThrow({
      where,
      select,
    });
  }

  // mutation
  async create(params: {
    data: Prisma.orderDetailCreateInput;
    select?: Prisma.orderDetailSelect;
  }): Promise<OrderDetail> {
    const { data, select } = params;
    return this.prisma.orderDetail.create({
      data,
      select,
    });
  }

  async update(params: {
    where: Prisma.orderDetailWhereUniqueInput;
    data: Prisma.orderDetailUpdateInput;
    select?: Prisma.orderDetailSelect;
  }): Promise<Partial<OrderDetail>> {
    const { where, data, select } = params;
    return this.prisma.orderDetail.update({
      where,
      data,
      select,
    });
  }

  async delete(params: {
    where: Prisma.orderDetailWhereUniqueInput;
    select?: Prisma.orderDetailSelect;
  }): Promise<OrderDetail> {
    const { where, select } = params;
    return this.prisma.orderDetail.delete({
      where,
      select,
    });
  }
}
