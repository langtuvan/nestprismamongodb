import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { CaslModule } from 'src/Casl/casl.module';
import { DomainService } from '../domain/domain.service';
import { CategoryService } from '../Category/category.service';

@Module({
  imports: [CaslModule],
  controllers: [OrderController],
  providers: [PrismaService, OrderService, DomainService],
})
export class OrderModule {}
