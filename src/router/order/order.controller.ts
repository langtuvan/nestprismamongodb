import {
  Controller,
  Get,
  UseGuards,
  NotFoundException,
  Res,
  Param,
  Body,
  Post,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
import { Response } from 'express';
// auth
import { AuthGuard } from 'src/guard/auth.guard';
import { RolesGuard } from 'src/guard/role.guard';
import { requiredRoles } from 'src/decorators/roles.decorator';
import { Role } from 'src/entries/role.enum';
// validate Dto
import { CreateCategoryDto, UpdateCategoryDto } from 'src/dto/category/';
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
// get decorateor
import { CurrentDomain } from 'src/decorators/domain.decorator';
import { CurrentUser } from 'src/decorators/user.decorator';
// service
import { OrderService } from './order.service';

import { order as OrderModel, Prisma } from '@prisma/client';

@UseGuards(AuthGuard, RolesGuard)
@requiredRoles(Role.USER)
@Controller()
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @Get('/')
  async getAll(
    @CurrentUser('domainId') domainId: string,
  ): Promise<Partial<OrderModel>[]> {
    return this.orderService.ordersInDomain({
      where: { id: domainId },
      include: {
        orders: {
          include: {
            customer: {
              select: {
                id: true,
                name: true,
                phone: true,
                email: true,
                isZalo: true,
                tinh_tp: true,
                quan_huyen: true,
                xa_phuong: true,
                dia_chi: true,
              },
            },
            details: {
              select: {
                id: true,
                price: true,
                qty: true,
                status: true,
                product: { select: { id: true, name: true } },
              },
            },
            logs: true,
            _count: true,
          },
        },
      },
    });
  }

  @Get('/:id')
  async get(
    @CurrentUser('domainId') domainId: string,
    @Param('id', CheckObjectIdPipe) id: string,
  ) {
    return this.orderService.order({
      where: { id, domainId },
      select: {
        id: true,
        customer: true,
        shipTo: true,
        details: {
          select: { id: true, product: { select: { name: true } } },
        },
        //details: { include: { product: true } },
      },
    });
  }

  @Post('/')
  async create(@CurrentDomain() domainId: string, @Body() body: any) {
    const { note, shipTo, customer, details } = body;

    const detailsUpdate: Prisma.orderDetailCreateWithoutOrderInput[] =
      details.map((detail) => ({
        price: detail.price,
        qty: detail.qty,
        product: { connect: { id: detail.productId } },
        domain: { connect: { id: domainId } },
      }));
    return this.orderService.create({
      data: {
        note,
        shipTo,
        customer: {
          connectOrCreate: {
            where: {
              phone: customer.phone,
            },
            create: {
              name: customer.name,
              phone: customer.phone,
              domain: { connect: { id: domainId } },
            },
          },
        },
        details: {
          create: detailsUpdate,
        },
        domain: { connect: { id: domainId } },
      },
      select: {
        id: true,
      },
    });
  }

  @Put('/:id')
  async update(
    @CurrentUser('domainId') domainId: string,
    @CurrentUser('id') updatedBy: string,
    @Param('id', CheckObjectIdPipe) id: string,
    @Body() body: any,
  ) {
    return this.orderService.update({
      where: { id, domainId },
      data: {
        ...body,
        updatedBy: { connect: { id: updatedBy } },
      },
    });
  }

  @Delete('/:id')
  async delete(
    @CurrentUser('domainId') domainId: string,
    @Param('id', CheckObjectIdPipe) id: string,
  ) {
    return this.orderService.delete({
      where: { id, domainId },
    });
  }
}
