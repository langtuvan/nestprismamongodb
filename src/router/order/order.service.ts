import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { order as Order, Prisma } from '@prisma/client';

@Injectable()
export class OrderService {
  constructor(private prisma: PrismaService) {}

  async ordersInDomain(params: {
    where: Prisma.domainWhereUniqueInput;
    include: Prisma.domainInclude;
  }): Promise<Order[]> {
    const { where, include } = params;
    const findDomain = await this.prisma.domain.findUniqueOrThrow({
      where,
      include,
    });
    return findDomain.orders;
  }

  async order(params: {
    where: Prisma.orderWhereUniqueInput;
    select?: Prisma.orderSelect;
  }): Promise<Partial<Order>> {
    const { where, select } = params;
    return this.prisma.order.findUniqueOrThrow({
      where,
      select,
    });
  }

  //   // mutation

  async create(params: {
    data: Prisma.orderCreateInput;
    select?: Prisma.orderSelect;
  }): Promise<Order> {
    const { data, select } = params;
    return this.prisma.order.create({
      data,
      select,
    });
  }

  async update(params: {
    where: Prisma.orderWhereUniqueInput;
    data: Prisma.orderUpdateInput;
    select?: Prisma.orderSelect;
  }): Promise<Partial<Order>> {
    const { where, data, select } = params;
    return this.prisma.order.update({
      where,
      data,
      select,
    });
  }

  async delete(params: {
    where: Prisma.orderWhereUniqueInput;
    select?: Prisma.orderSelect;
  }): Promise<Order> {
    const { where, select } = params;

    await this.prisma.order.update({
      where,
      data: {
        details: { deleteMany: {} },
      },
    });

    return this.prisma.order.delete({
      where,
      select,
    });
  }
}
