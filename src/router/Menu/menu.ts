import {
  ManagementNavigation,
  OverviewNavigation,
  MenuType,
  OverviewMenu,
} from 'src/types/menu';

//menu
const root = 'dashboard';
const path = (module: string, action?: string) =>
  !action ? `/${root}/${module}` : `/${root}/${module}/${action}`;

const router = {
  User: 'user',
  Category: 'category',
};

const overviewNavigation: OverviewMenu = {
  app: {
    displayNameEN: 'App',
    displayNameVi: 'Ứng Dụng',
    href: `/dashboard/app`,
    icon: 'HomeIcon',
  },
  calendar: {
    displayNameEN: 'Calendar',
    displayNameVi: 'Lịch',
    href: `/dashboard/calendar`,
    icon: 'HomeIcon',
    children: [
      { title: 'Day view', href: 'day' },
      { title: 'Week view', href: 'week' },
      { title: 'Month view', href: 'month' },
      { title: 'Year view', href: 'year' },
    ],
  },
};
const managementNavigation: ManagementNavigation[] = [
  {
    displayNameVi: 'Nhân Viên',
    displayNameEN: 'User',
    href: router.User,
    icon: 'UsersIcon',
    children: [
      {
        displayNameEN: 'List User',
        displayNameVi: 'Danh Sách',
        href: path(router.User, 'list'),
      },
      // { name: "Add", href: path(router.User, action.Add) },
    ],
  },
  {
    displayNameVi: 'Thương Mại',
    displayNameEN: 'Ecomerce',
    href: 'Ecomerce',
    icon: 'CubeIcon',

    children: [
      {
        displayNameEN: 'Category',
        displayNameVi: 'Danh Mục',
        href: path(router.Category),
      },
      // { name: "SubCategories", href: path(router.SubCategory) },
      // { name: "Products", href: path(router.Product) },
    ],
  },
];

export const menu: MenuType = {
  overviewNavigation,
  managementNavigation,
};

export default menu;
