import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
//import { user as User, Prisma } from '@prisma/client';
import { MenuType } from 'src/types/menu';
import menu from './menu';

@Injectable()
export class MenuService {
  constructor(private prisma: PrismaService) {}

  async menus(): Promise<MenuType> {
    return menu;
  }
}
