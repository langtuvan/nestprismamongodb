import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { BrandController } from './brand.controller';
import { BrandService } from './brand.service';
import { CaslModule } from 'src/Casl/casl.module';
import { DomainService } from '../domain/domain.service';

@Module({
  imports: [CaslModule],
  controllers: [BrandController],
  providers: [PrismaService, BrandService, DomainService],
})
export class BrandModule {}
