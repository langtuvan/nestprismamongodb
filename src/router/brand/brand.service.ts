import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { brand as Brand, Prisma } from '@prisma/client';

@Injectable()
export class BrandService {
  constructor(private prisma: PrismaService) {}
  async brands(params: {
    where: Prisma.domainWhereUniqueInput;
    include?: Prisma.domainInclude;
  }): Promise<Brand[]> {
    const { where, include } = params;
    const findDomain = await this.prisma.domain.findUniqueOrThrow({
      where,
      include,
    });

    return findDomain.brands;
  }
}
