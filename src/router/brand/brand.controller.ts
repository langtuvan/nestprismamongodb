import {
  Controller,
  Get,
  UseGuards,
  Param,
  Body,
  Post,
  Put,
  Delete,
} from '@nestjs/common';
// auth
import { AuthGuard } from 'src/guard/auth.guard';
import { requiredRoles } from 'src/decorators/roles.decorator';
import { Role } from 'src/entries/role.enum';
// import { PoliciesGuard } from 'src/guard/policies.guard';
// import { CheckPolicies } from 'src/decorators/policies.decorator';
// import {
//   AppAbility,
//   CaslAbilityFactory,
//   User,
// } from 'src/Casl/casl-ability.factory';
// import Action from 'src/entries/permission/action.enum';

// validate Dto
import { CreateCategoryDto, UpdateCategoryDto } from 'src/dto/category/';
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
// get decorateor
import { CurrentDomain } from 'src/decorators/domain.decorator';
import { CurrentUser } from 'src/decorators/user.decorator';
// service
import { BrandService } from './brand.service';
import { category as CategoryModel } from '@prisma/client';
import { RolesGuard } from 'src/guard/role.guard';

@UseGuards(AuthGuard, RolesGuard)
@requiredRoles(Role.USER)
@Controller()
export class BrandController {
  constructor(private readonly brandService: BrandService) {}

  @Get('/')
  async getAllByDomainKey(
    @CurrentUser('domainId') domainId: string,
  ): Promise<Partial<CategoryModel>[]> {
    return this.brandService.brands({
      where: { id: domainId },
      include: {
        brands: true,
        //_count: true,
      },
    });
  }
}
