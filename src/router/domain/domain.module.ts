import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { DomainController } from './domain.controller';
import { DomainService } from './domain.service';
import { CaslModule } from 'src/Casl/casl.module';
import { UserService } from '../User/user.service';

@Module({
  imports: [CaslModule],
  controllers: [DomainController],
  providers: [PrismaService, DomainService, UserService],
})
export class DoaminModule {}
