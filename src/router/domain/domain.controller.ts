import {
  Controller,
  Get,
  UseGuards,
  Param,
  Post,
  Put,
  Body,
  BadRequestException,
} from '@nestjs/common';
// auth // guard
import { AuthGuard } from 'src/guard/auth.guard';
import { StaffRolesGuard } from 'src/guard/staffRole.guard';
import { staffRequiredRoles } from 'src/decorators/roles.decorator';
import { StaffRole } from 'src/entries/role.enum';
// validate Dto
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
import { CreateDomainDto, UpdateDomainDto } from 'src/dto/domain';
// get decorateor
import { CurrentUser } from 'src/decorators/user.decorator';
import { CurrentDomain } from 'src/decorators/domain.decorator';
// service
import { DomainService } from './domain.service';
import { UserService } from '../User/user.service';
// type
import { domain as DomainModel, Prisma } from '@prisma/client';
// ultils
import { addMonths } from 'date-fns';

const TRIAL_MONTH = 3;

@UseGuards(AuthGuard, StaffRolesGuard)
@staffRequiredRoles(StaffRole.SUPPERADMIN)
@Controller()
export class DomainController {
  constructor(
    private readonly domainService: DomainService,
    private readonly userService: UserService,
  ) {}

  @Get('/')
  async getAll(
    @CurrentUser('domainId') domainId: string,
  ): Promise<Partial<DomainModel>[]> {
    return this.domainService.domains({});
  }

  @Get('/:id')
  async get(
    @Param('id', CheckObjectIdPipe) id: string,
  ): Promise<Partial<DomainModel>> {
    return this.domainService.domain({
      where: { id },
    });
  }

  //mutation
  @staffRequiredRoles(StaffRole.SALE, StaffRole.USER, StaffRole.MANAGER)
  @Post('/')
  async create(
    @Body() body: CreateDomainDto,
    @CurrentUser('id') staffId: string,
  ): Promise<Partial<DomainModel>> {
    const { name, status, plan, user,  categories } = body;
    return this.domainService.createDomain({
      data: {
        name,
        status: 'trial',
        activeDate: new Date(),
        expDate: addMonths(new Date(), TRIAL_MONTH),
        staff: { connect: { id: staffId } },
        plan: {
          connect: {
            id: plan as string,
          },
        },
        users: {
          create: {
            ...user,
            role: [StaffRole.ADMIN],
            isAdmin: true,
            active: true,
            password: await this.userService.CreatePasswordHash(
              user?.password || '123456',
            ),
          },
        },
        bill: {
          create: {
            plan: { connect: { id: plan } },
            amount: 750,
            createdBy: { connect: { id: staffId } },
          },
        },
      },
    });
  }

  @Put('/:id')
  async update(
    @Param('id', CheckObjectIdPipe) id: string,
    @Body() data: Prisma.domainUpdateInput,
  ): Promise<Partial<DomainModel>> {
    return this.domainService.update({ where: { id }, data });
  }

  @staffRequiredRoles(StaffRole.SALE, StaffRole.USER, StaffRole.MANAGER)
  @Put('/change-plan-domain/:id')
  async changePlan(
    @Param('id', CheckObjectIdPipe) domainId: string,
    @Body() data: UpdateDomainDto,
    @CurrentUser('id') staffId: string,
  ) {
    const { plan } = data;

    const domain = await this.domainService.domain({
      where: { id: domainId },
      include: {
        bill: {
          where: { planId: { equals: plan }, domainId: { equals: domainId } },
        },
      },
    })

    if (domain.bill.length > 0) {
      throw new BadRequestException('bill has existing with this plan !');
    }

    const getPlan = await this.domainService.getPlan({
      where: { id: plan },
    });

    return this.domainService.update({
      where: { id: domainId },
      data: {
        bill: {
          create: {
            plan: { connect: { id: getPlan.id } },
            amount: getPlan.price.monthly * 12,
            createdBy: { connect: { id: staffId } },
          },
        },
      },
    });
  }
}
