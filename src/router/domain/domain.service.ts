import {
  BadRequestException,
  NotFoundException,
  Injectable,
} from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { domain as Domain, Prisma } from '@prisma/client';

@Injectable()
export class DomainService {
  constructor(private prisma: PrismaService) {}

  async domains(params: {
    where?: Prisma.domainWhereInput;
    include?: Prisma.domainInclude;
    skip?: number;
    take?: number;
    cursor?: Prisma.domainWhereUniqueInput;
    orderBy?: Prisma.domainOrderByWithRelationInput;
  }): Promise<Partial<Domain>[]> {
    const { where, include, skip, take, cursor, orderBy } = params
    return this.prisma.domain.findMany({
      where,
      skip,
      take,
      cursor,
      include,
      orderBy,
    });
  }

  async domain(params: {
    where: Prisma.domainWhereUniqueInput;
    include?: Prisma.domainInclude;
  }) {
    const { where, include } = params;
    return this.prisma.domain.findUniqueOrThrow({
      where,
      include,
    });
  }

  async getPlan(params: { where: Prisma.planWhereUniqueInput }) {
    const { where } = params;
    return this.prisma.plan.findUniqueOrThrow({
      where,
    });
  }

  async createDomain(params: {
    data: Prisma.domainCreateInput;
  }): Promise<Partial<Domain>> {
    return this.prisma.domain.create({
      data: params.data,
    });
  }

  async update(params: {
    where: Prisma.domainWhereUniqueInput;
    data: Prisma.domainUpdateInput;
    select?: Prisma.domainSelect;
  }): Promise<Partial<Domain>> {
    const { where, data, select } = params;
    const update = this.prisma.domain.update({
      where,
      data,
      select,
    });
    if (!update) {
      throw new BadRequestException('update failed');
    }
    return update;
  }
}
