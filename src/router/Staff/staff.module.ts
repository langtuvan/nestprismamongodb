import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { StaffController } from './staff.controller';
import { CaslModule } from 'src/Casl/casl.module';
import { StaffService } from './staff.service';


@Module({
  imports: [CaslModule],
  controllers: [StaffController],
  providers: [PrismaService, StaffService],
})
export class StaffModule {}
