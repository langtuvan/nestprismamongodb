import {
  Controller,
  Get,
  UseGuards,
  NotFoundException,

  Res,
  Param,
  Body,
  Post,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
import { Response } from 'express';
// auth // guard
import { AuthGuard } from 'src/guard/auth.guard';
import { StaffRolesGuard } from 'src/guard/staffRole.guard';
import { staffRequiredRoles } from 'src/decorators/roles.decorator';
import { StaffRole } from 'src/entries/role.enum';
// validate Dto
import { CreateCategoryDto, UpdateCategoryDto } from 'src/dto/category/';
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
import { CreateUserDto } from 'src/dto/user';
// get decorateor
import { CurrentDomain } from 'src/decorators/domain.decorator';
import { CurrentUser } from 'src/decorators/user.decorator';
// service
import { StaffService } from './staff.service';
// type
import { staff as StaffModel } from '@prisma/client';

@UseGuards(AuthGuard, StaffRolesGuard)
@staffRequiredRoles(StaffRole.SUPPERADMIN)
@Controller()
export class StaffController {
  constructor(private readonly staffService: StaffService) {}

  @Post()
  async Create(
    @Body() body: any, // not yet define DTO
    @CurrentUser('id') createdBy: string,
  ): Promise<Partial<StaffModel>> {
    const { password, ...data } = body;

    await this.staffService.checkStaffExitting({
      email: data.email,
      phone: data.phone,
      codeStaff: data.codeStaff,
    });

    return this.staffService.create({
      data: {
        ...data,
        password: await this.staffService.CreatePasswordHash(password),
      },
    });
  }
}
