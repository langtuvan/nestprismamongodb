import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { staff as Staff, Prisma } from '@prisma/client';
import * as bcrypt from 'bcrypt';
import { ErrorMessage } from 'src/types/error';

const saltRounds = 10;

@Injectable()
export class StaffService {
  constructor(private prisma: PrismaService) {}

  async staff(params: { where: Prisma.staffWhereUniqueInput }) {
    const { where } = params;
    const find = await this.prisma.staff.findUnique({ where });
    if (!find) {
      throw new NotFoundException();
    }
    return find;
  }

  // mutation
  async create(params: { data: Prisma.staffCreateInput }): Promise<Staff> {
    const { data } = params;
    return this.prisma.staff.create({
      data,
    });
  }

  //check Email and Phone exitting
  async checkStaffExitting(staffWhereInput: Prisma.staffWhereInput) {
    const staffs = await this.prisma.staff.findMany({
      where: {
        OR: [
          { codeStaff: { equals: staffWhereInput.codeStaff as string } },
          { email: { equals: staffWhereInput.email as string } },
          { phone: { equals: staffWhereInput.phone as string } },
        ],
      },
    });

    let errorMessage: ErrorMessage[] = [];

    if (
      staffs &&
      staffs.length > 0 &&
      staffs
        .map((u) => u.codeStaff)
        .includes(staffWhereInput.codeStaff as string)
    ) {
      errorMessage = [
        ...errorMessage,
        {
          field: 'codeStaff',
          message: `codeStaff: ${staffWhereInput.codeStaff} is exitsing.`,
        },
      ];
    }

    if (
      staffs &&
      staffs.length > 0 &&
      staffs.map((u) => u.phone).includes(staffWhereInput.phone as string)
    ) {
      errorMessage = [
        ...errorMessage,
        {
          field: 'phone',
          message: `phone: ${staffWhereInput.phone} is exitsing.`,
        },
      ];
    }

    if (
      staffs &&
      staffs.length > 0 &&
      staffs.map((u) => u.email).includes(staffWhereInput.email as string)
    ) {
      errorMessage = [
        ...errorMessage,
        {
          field: 'email',
          message: `email: ${staffWhereInput.email} is exitsing.`,
        },
      ];
    }

    if (errorMessage.length > 0) {
      throw new BadRequestException(errorMessage);
    }
  }

  // compare password
  async comparePassword(
    myPlaintextPassword: string,
    passwordInDb: string,
  ): Promise<boolean> {
    return bcrypt.compare(myPlaintextPassword, passwordInDb);
  }

  // create password hash
  async CreatePasswordHash(myPlaintextPassword: string): Promise<string> {
    return bcrypt.hash(myPlaintextPassword, saltRounds);
  }
}
