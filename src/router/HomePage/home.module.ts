import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { HomePageController } from './home.controller';

@Module({
  controllers: [HomePageController],
  //providers: [PrismaService],
})
export class HomePageModule {}
