import { Controller, Get } from '@nestjs/common';
import { Throttle } from '@nestjs/throttler';

@Controller()
export class HomePageController {
  constructor() {}
  // @Throttle({ default: { limit: 5, ttl: 60000 } })
  @Get('')
  homepage() {
    return 'Api backend. Contact email: langtuvan@hotmail.com , phone: +84 938 887 467, address : District 11, HoChiMinh city, VietNam';
  }
}
