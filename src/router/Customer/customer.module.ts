import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { CustomerController } from './customer.controller';
import { CustomerService } from './customer.service';
import { CaslModule } from 'src/Casl/casl.module';
import { DomainService } from '../domain/domain.service';

@Module({
  imports: [CaslModule],
  controllers: [CustomerController],
  providers: [PrismaService, CustomerService, DomainService],
})
export class CustomerModule {}
