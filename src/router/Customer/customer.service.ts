import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { Customer as Customer, Prisma } from '@prisma/client';

@Injectable()
export class CustomerService {
  constructor(private prisma: PrismaService) {}

  async customersInDomain(params: {
    where: Prisma.domainWhereUniqueInput;
  }): Promise<Customer[]> {
    const { where } = params;
    return this.prisma.domain.findUniqueOrThrow({ where }).customers();
  }

  async customer(params: {
    where: Prisma.CustomerWhereUniqueInput;
    select?: Prisma.CustomerSelect;
  }): Promise<Partial<Customer>> {
    const { where, select } = params;
    return this.prisma.customer.findUniqueOrThrow({
      where,
      select,
    });
  }

  // mutation

  async create(params: {
    data: Prisma.CustomerCreateInput;
    select?: Prisma.CustomerSelect;
  }): Promise<Customer> {
    const { data, select } = params;
    return this.prisma.customer.create({
      data,
      select,
    });
  }

  async update(params: {
    where: Prisma.CustomerWhereUniqueInput;
    data: Prisma.CustomerUpdateInput;
    select?: Prisma.CustomerSelect;
  }): Promise<Partial<Customer>> {
    const { where, data, select } = params;
    return this.prisma.customer.update({
      where,
      data,
      select,
    });
  }

  async delete(params: {
    where: Prisma.CustomerWhereUniqueInput;
    select?: Prisma.CustomerSelect;
  }): Promise<Customer> {
    const { where, select } = params;
    return this.prisma.customer.delete({
      where,
      select,
    });
  }
}
