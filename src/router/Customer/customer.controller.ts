import {
  Controller,
  Get,
  UseGuards,
  NotFoundException,
  Res,
  Param,
  Body,
  Post,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
import { Response } from 'express';
// auth
import { AuthGuard } from 'src/guard/auth.guard';
import { requiredRoles } from 'src/decorators/roles.decorator';
import { Role } from 'src/entries/role.enum';
// validate Dto
import { CreateCategoryDto, UpdateCategoryDto } from 'src/dto/category/';
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
// get decorateor
import { CurrentDomain } from 'src/decorators/domain.decorator';
import { CurrentUser } from 'src/decorators/user.decorator';
// service
import { CustomerService } from './customer.service';

import { Customer as CustomerModel } from '@prisma/client';

@UseGuards(AuthGuard)
@requiredRoles(Role.USER)
@Controller()
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @Get('/')
  async getAll(
    @CurrentUser('domainId') domainId: string,
  ): Promise<Partial<CustomerModel>[]> {
    return this.customerService.customersInDomain({
      where: { id: domainId },
    });
  }

  @Get('/:id')
  async get(
    @CurrentUser('domainId') domainId: string,
    @Param('id', CheckObjectIdPipe) id: string,
  ): Promise<Partial<CustomerModel>> {
    return this.customerService.customer({
      where: { id, domainId },
    });
  }

  @Post('/')
  async create(
    @CurrentUser('domainId') domainId: string,
    @Body() body: any,
  ): Promise<Partial<CustomerModel>> {
    const { ...data } = body;
    return this.customerService.create({
      data: {
        ...data,
        domain: { connect: { id: domainId } },
      },
    });
  }

  @Put('/:id')
  async update(
    @CurrentUser('domainId') domainId: string,
    @Param('id', CheckObjectIdPipe) id: string,
    @Body() body: any,
  ) {}

  @Delete('/id')
  async delete(
    @CurrentUser('domainId') domainId: string,
    @Param('id', CheckObjectIdPipe) id: string,
  ) {}
}
