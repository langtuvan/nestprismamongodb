import {
  Controller,
  Get,
  UseGuards,
  Param,
  Body,
  Post,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
// auth
import { AuthGuard } from 'src/guard/auth.guard';
import { requiredRoles } from 'src/decorators/roles.decorator';
import { Role } from 'src/entries/role.enum';
// import { PoliciesGuard } from 'src/guard/policies.guard';
// import { CheckPolicies } from 'src/decorators/policies.decorator';
// import {
//   AppAbility,
//   CaslAbilityFactory,
//   User,
// } from 'src/Casl/casl-ability.factory';
// import Action from 'src/entries/permission/action.enum';

// validate Dto
import { CreateCategoryDto, UpdateCategoryDto } from 'src/dto/category/';
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
// get decorateor
import { CurrentDomain } from 'src/decorators/domain.decorator';
import { CurrentUser } from 'src/decorators/user.decorator';
// service
import { CategoryService } from './category.service';
import { category as CategoryModel } from '@prisma/client';
import { RolesGuard } from 'src/guard/role.guard';

@Controller()
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @UseGuards(AuthGuard, RolesGuard)
  @requiredRoles(Role.USER)
  @Get('/')
  async getAllByDomainKey(
    @CurrentUser('domainId') domainId: string,
  ): Promise<Partial<CategoryModel>[]> {
    return this.categoryService.categories({
      where: { domainId: domainId },
      include: {
        _count: true,
      },
    });
  }

  @Get('/getSlug')
  async getAllMain(
    // @CurrentDomain(CheckObjectIdPipe) CurrentDomain: string,
  ): Promise<Partial<CategoryModel>[]> {


    return this.categoryService.Main({
      where: { id: '654b3060e4abcf8683ed5752' },
    });
  }

  @Get('/main/:href')
  async MainHref(
    @Param('href') href: string,
    //@CurrentDomain(CheckObjectIdPipe) CurrentDomain: string,
  ): Promise<Partial<CategoryModel>> {
    return this.categoryService.category({
      where: { href },
      include: { products: true },
    });
  }

  @requiredRoles()
  @Get('/:catId')
  async get(
    @Param('catId', CheckObjectIdPipe) id: string,
    @CurrentUser('domainId') domainId: string,
  ): Promise<Partial<CategoryModel>> {
    return this.categoryService.category({
      where: { id, domainId },
      include: { _count: true },
    });
  }

  // mutation
  @UseGuards(AuthGuard, RolesGuard)
  @requiredRoles(Role.USER)
  @Post()
  async Create(
    @CurrentUser('id') createdBy: string,
    @CurrentUser('domainId') domainId: string,
    @Body() body: CreateCategoryDto,
  ): Promise<Partial<CategoryModel>> {
    const { ...data } = body;

    return this.categoryService.create({
      data: {
        ...data,
        domain: { connect: { id: domainId } },
        createdBy: { connect: { id: createdBy } },
      },
      include: { _count: true },
    });
  }

  @UseGuards(AuthGuard, RolesGuard)
  @requiredRoles(Role.USER)
  @Put('/:id')
  async Update(
    @CurrentUser('domainId') domainId: string,
    @CurrentUser('id') updatedBy: string,
    @Param('id', CheckObjectIdPipe) id: string,
    @Body() body: UpdateCategoryDto,
  ): Promise<Partial<any>> {
    const { ...data } = body;
    return this.categoryService.update({
      where: { id, domainId },
      data: {
        ...data,
        updatedBy: { connect: { id: updatedBy } },
      },
      include: { _count: true },
    });
  }

  @UseGuards(AuthGuard, RolesGuard)
  @requiredRoles(Role.USER)
  @Delete('/:id')
  async Delete(
    @Param('id', CheckObjectIdPipe) id: string,
    @CurrentUser('domainId') domainId: string,
  ): Promise<Partial<CategoryModel>> {
    return this.categoryService.delete({
      where: { id, domainId },
    });
  }
}
