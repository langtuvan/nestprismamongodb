import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { category as Category, Prisma } from '@prisma/client';
import { includes } from 'lodash';

@Injectable()
export class CategoryService {
  constructor(private prisma: PrismaService) {}

  async Main(params: {
    where: Prisma.domainWhereUniqueInput;
    select?: Prisma.domainSelect;
  }): Promise<Category[]> {
    const { where, select } = params;
    const domain = await this.prisma.domain.findUniqueOrThrow({
      where,
      include: { categories: true },
      //select,
    });

    return domain.categories;
  }

  async categories(params: {
    where: Prisma.categoryWhereInput;
    include?: Prisma.categoryInclude;
  }): Promise<Category[]> {
    const { where, include } = params;
    return await this.prisma.category.findMany({
      where,
      include,
    });
  }

  async category(params: {
    where: Prisma.categoryWhereUniqueInput;
    include?: Prisma.categoryInclude;
    select?: Prisma.categorySelect;
  }): Promise<Partial<Category>> {
    const { where, include } = params;
    return this.prisma.category.findUniqueOrThrow({
      where,
      include,
    });
  }

  // mutation
  async create(params: {
    data: Prisma.categoryCreateInput;
    include?: Prisma.categoryInclude;
    select?: Prisma.categorySelect;
  }): Promise<Category> {
    const { data, include } = params;
    return this.prisma.category.create({
      data,
      include,
    });
  }

  async update(params: {
    where: Prisma.categoryWhereUniqueInput;
    data: Prisma.categoryUpdateInput;
    select?: Prisma.categorySelect;
    include?: Prisma.categoryInclude;
  }): Promise<Category> {
    const { where, data, include } = params;
    return this.prisma.category.update({
      where,
      data,
      include,
    });
  }

  async delete(params: {
    where: Prisma.categoryWhereUniqueInput;
    select?: Prisma.categorySelect;
  }): Promise<Category> {
    const { where, select } = params;
    return this.prisma.category.delete({
      where,
      select,
    });
  }
}
