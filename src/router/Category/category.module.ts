import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { CaslModule } from 'src/Casl/casl.module';
import { DomainService } from '../domain/domain.service';

@Module({
  imports: [CaslModule],
  controllers: [CategoryController],
  providers: [PrismaService, CategoryService, DomainService],
})
export class CategoryModule {}
