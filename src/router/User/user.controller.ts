import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  Delete,
  UseGuards,
  BadRequestException,
} from '@nestjs/common';
import { Response } from 'express';
import { UserService } from './user.service';
import { Prisma, user as UserModel } from '@prisma/client';
// Guard
import { AuthGuard } from 'src/guard/auth.guard';
import { RolesGuard } from 'src/guard/role.guard';
import { Role } from 'src/entries/role.enum';
import { requiredRoles } from 'src/decorators/roles.decorator';
// validate
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
import { CurrentUser } from 'src/decorators/user.decorator';
import { CreateUserDto, UpdateUserDto } from 'src/dto/user';
import { CurrentDomain } from 'src/decorators/domain.decorator';

@Controller()
@UseGuards(AuthGuard, RolesGuard)
@requiredRoles(Role.ADMIN)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('/')
  async getAll(
    @CurrentUser('domainId') domainId: string,
  ): Promise<Partial<UserModel>[]> {
    return this.userService.users({ where: { id: domainId } });
  }

  @Get('/:id')
  async get(
    @CurrentUser('domainId') domainId: string,
    @Param('id', CheckObjectIdPipe) id: string,
  ) {
    return this.userService.user({ where: { id, domainId } });
  }

  //mutation
  @requiredRoles(Role.ADMIN)
  @Post()
  async Create(
    @CurrentUser('domainId') domainId: string,
    @CurrentUser('id') createdBy: string,
    @Body() body: CreateUserDto,
  ): Promise<Partial<UserModel>> {
    const { password, ...data } = body;

    await this.userService.checkUserExitting({
      where: { id: domainId },
      userWhereInput: { email: data.email, phone: data.phone },
    });

    return this.userService.create({
      data: {
        ...data,
        password: await this.userService.CreatePasswordHash(
          password || '123456',
        ),
        domain: { connect: { id: domainId } },
        createdBy,
      },
    });
  }

  @Put('/:id')
  async update(
    @Param('id', CheckObjectIdPipe) id: string,
    @CurrentUser('id') updatedBy: string,
    @CurrentUser('domainId') domainId: string,
    @Body() updateUserData: UpdateUserDto,
  ): Promise<Partial<UserModel>> {
    const { password, ...userCanUpdate } = updateUserData;

    await this.userService.user({ where: { id, domainId } });

    let data: Partial<UserModel>;

    if (password) {
      data = {
        ...userCanUpdate,
        updatedBy,
        password: await this.userService.CreatePasswordHash(password),
      };
    } else {
      data = {
        ...userCanUpdate,
        updatedBy,
      };
    }

    return this.userService.update({
      where: { id, domainId },
      data,
    });
  }

  @Delete('/:id')
  async deleteOne(
    @Param('id', CheckObjectIdPipe) id: string,
    @CurrentUser('domainId') domainId: string,
    @CurrentUser('id') deletedBy: string,
  ): Promise<UserModel> {
    const user = await this.userService.user({ where: { id } });

    if (user.role.includes(Role.ADMIN)) {
      throw new BadRequestException(
        'You can not delete a user has admin role !',
      );
    }

    return this.userService.delete({
      where: { id, domainId },
    });
  }

  // @Post('/delete')
  // async deleteMany(
  //   @CurrentUser('id') deletedBy: string,
  //   @Body() arrayId: string[],
  // ): Promise<Response<UserModel>> {

  //   if (arrayId.some((u) => DEFAULT_USERS.includes(u))) {
  //     throw new BadRequestException(
  //       'THIS IS EXAMPLE DEFAULT USER, YOU CAN NOT UPDATE OR DELETE !',
  //     );
  //   }

  //   const users = await this.userService.users({
  //     where: {
  //       id: {
  //         in: arrayId,
  //       },
  //     },
  //   });
  //   const userOnly = users.filter((user) => !user.role.includes('admin'));

  //   if (userOnly.length === 0) {
  //     throw new BadRequestException(
  //       'No user to delete Or the user selected can have admin Role',
  //     );
  //   }
  //   return this.userService.deleteUsers(
  //     userOnly.map((u) => u.id),
  //     deletedBy,
  //   );
  // }
}
