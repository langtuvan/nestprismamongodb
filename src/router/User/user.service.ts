import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { user as User, domain as Domain, Prisma } from '@prisma/client';
import * as bcrypt from 'bcrypt';
import { ErrorMessage } from 'src/types/error';
import _ from 'lodash';

const saltRounds = 10;

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  async users(params: {
    where?: Prisma.domainWhereInput;
    // skip?: number;
    // take?: number;
    // cursor?: Prisma.userWhereUniqueInput;
    // where?: Prisma.userWhereInput;
    // orderBy?: Prisma.userOrderByWithRelationInput;
    // select?: Prisma.userSelect;
  }): Promise<Partial<User>[]> {
    const { where } = params;
    //const { skip, take, cursor, where, orderBy, select } = params;
    return this.prisma.domain
      .findFirstOrThrow({
        where,
      })
      .users();
  }

  async user(params: {
    where: Prisma.userWhereInput;
    select?: Prisma.userSelect;
  }): Promise<Partial<User> | null> {
    const { where, select } = params;
    return this.prisma.user.findFirstOrThrow({
      where,
      select,
    });
  }

  async create(params: {
    data: Prisma.userCreateInput;
    select?: Prisma.userSelect;
  }): Promise<any> {
    const { data, select } = params;
    return this.prisma.user.create({
      data,
      select,
    });
  }

  async update(params: {
    where: Prisma.userWhereUniqueInput;
    data: Prisma.userUpdateInput;
    select?: Prisma.userSelect;
  }): Promise<User> {
    const { where, data, select } = params;
    return this.prisma.user.update({
      where,
      data,
      select,
    });
  }

  async delete(params: {
    where: Prisma.userWhereUniqueInput;
    select?: Prisma.userSelect;
  }): Promise<User> {
    const { where, select } = params;
    return this.prisma.user.delete({
      where,
    });
  }

  // async deleteUsers(arrayId: string[]): Promise<any> {
  //   return this.prisma.user.deleteMany({
  //     where: {
  //       id: {
  //         in: arrayId,
  //       },
  //     },
  //   });
  // }

  //check Email and Phone exitting
  async checkUserExitting(params: {
    where: Prisma.domainWhereUniqueInput;
    userWhereInput: Prisma.userWhereInput;
  }) {
    const { where, userWhereInput } = params;
    const users = await this.prisma.domain
      .findUnique({
        where,
      })
      .users();

    // const users = await this.prisma.user.findMany({
    //   where: {
    //     OR: [
    //       { email: { equals: userWhereInput.email as string } },
    //       { phone: { equals: userWhereInput.phone as string } },
    //     ],
    //   },
    // });

    if (!users) {
      return;
    }

    let errorMessage: ErrorMessage[] = [];

    if (users.map((u) => u.phone).includes(userWhereInput.phone as string)) {
      errorMessage = [
        ...errorMessage,
        {
          field: 'phone',
          message: `phone: ${userWhereInput.phone} is exitsing!`,
        },
      ];
    }

    if (users.map((u) => u.email).includes(userWhereInput.email as string)) {
      errorMessage = [
        ...errorMessage,
        {
          field: 'email',
          message: `email: ${userWhereInput.email} is exitsing!`,
        },
      ];
    }

    if (errorMessage.length > 0) {
      throw new BadRequestException(errorMessage);
    }
  }

  // compare password
  async comparePassword(
    myPlaintextPassword: string,
    passwordInDb: string,
  ): Promise<boolean> {
    return bcrypt.compare(myPlaintextPassword, passwordInDb);
  }

  // create password hash
  async CreatePasswordHash(myPlaintextPassword: string): Promise<string> {
    return bcrypt.hash(myPlaintextPassword, saltRounds);
  }
}
