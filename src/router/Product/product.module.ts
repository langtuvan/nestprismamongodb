import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';
import { CaslModule } from 'src/Casl/casl.module';
import { DomainService } from '../domain/domain.service';
import { CategoryService } from '../Category/category.service';

@Module({
  imports: [CaslModule],
  controllers: [ProductController],
  providers: [PrismaService, ProductService, DomainService, CategoryService],
})
export class ProductModule {}
