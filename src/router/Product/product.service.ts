import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { product as Product, Prisma } from '@prisma/client';

@Injectable()
export class ProductService {
  constructor(private prisma: PrismaService) {}

  async Main(params: {
    where: Prisma.domainWhereUniqueInput;
    select?: Prisma.domainSelect;
  }): Promise<Product[]> {
    const { where, select } = params;

    const domain = await this.prisma.domain.findUniqueOrThrow({
      where,
      select,
    });
    return domain.products;
  }

  async productsInDomain(params: {
    where: Prisma.domainWhereUniqueInput;
  }): Promise<Product[]> {
    const { where } = params;

    const domain = await this.prisma.domain.findUniqueOrThrow({
      where,
      include: { products: { include: { category: true, brand: true } } },
    });
    return domain.products;
  }

  async productsInCategory(params: {
    where: Prisma.categoryWhereUniqueInput;
  }): Promise<Product[]> {
    const { where } = params;
    return this.prisma.category.findUniqueOrThrow({ where }).products();
  }

  async product(params: {
    where: Prisma.productWhereUniqueInput;
    include?: Prisma.productInclude;
  }): Promise<Partial<Product> | null> {
    const { where, include } = params;
    return this.prisma.product.findUniqueOrThrow({
      where,
      include,
    });
  }

  // mutation

  async create(params: {
    data: Prisma.productCreateInput;
    include?: Prisma.productInclude;
  }): Promise<Product> {
    const { data, include } = params;
    return this.prisma.product.create({
      data,
      include,
    });
  }

  async update(params: {
    where: Prisma.productWhereUniqueInput;
    data: Prisma.productUpdateInput;
    include?: Prisma.productInclude;
    //select?: Prisma.productSelect;
  }): Promise<Partial<Product>> {
    const { where, data, include } = params;
    return this.prisma.product.update({
      where,
      data,
      include,
    });
  }

  async delete(params: {
    where: Prisma.productWhereUniqueInput;
    select?: Prisma.productSelect;
  }): Promise<Product> {
    const { where, select } = params;
    return this.prisma.product.delete({
      where,
      select,
    });
  }
}
