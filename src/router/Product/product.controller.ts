import {
  Controller,
  Get,
  UseGuards,
  NotFoundException,
  Res,
  Param,
  Body,
  Post,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
import { Response } from 'express';
// auth
import { AuthGuard } from 'src/guard/auth.guard';
import { requiredRoles } from 'src/decorators/roles.decorator';
import { Role } from 'src/entries/role.enum';
// validate Dto
import { CreateCategoryDto, UpdateCategoryDto } from 'src/dto/category/';
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
// get decorateor
import { CurrentDomain } from 'src/decorators/domain.decorator';
import { CurrentUser } from 'src/decorators/user.decorator';
// service
import { ProductService } from './product.service';

import { Prisma, product as ProductModel } from '@prisma/client';
import CreateProductDto from 'src/dto/product/create-product.dto';
import UpdateProductDto from 'src/dto/product/update-product.dto';

@Controller()
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get('/')
  async getAllByDomainKey(
    @Query('catId') catId: string,
    @CurrentUser('domainId') domainId: string,
    //@CurrentDomain(CheckObjectIdPipe) CurrentDomain: string,
  ): Promise<Partial<ProductModel>[]> {
    if (!catId) {
      return this.productService.productsInDomain({
        where: { id: domainId },
      });
    }
    return this.productService.productsInCategory({
      where: { id: domainId },
    });
  }

  @Get('/main')
  async main(
    @Query('query') query: string,
    @CurrentDomain(CheckObjectIdPipe) CurrentDomain: string,
  ): Promise<Partial<ProductModel>[]> {
    if (query === 'generateStaticParams') {
      return this.productService.Main({
        where: { id: '654b3060e4abcf8683ed5752' },
        select: {
          products: {
            select: { slug: true },
          },
        },
      });
    }
    return this.productService.productsInDomain({
      where: { id: '654b3060e4abcf8683ed5752' },
    });
  }

  @Get('/main/:slug')
  async mainSlug(
    @Param('slug') slug: string,
  ): Promise<Partial<ProductModel>[] | Partial<ProductModel>> {
    return this.productService.product({
      where: { slug },
      include: { category: true, brand: true },
    });
  }

  @Get('/:id')
  async get(
    @Param('id', CheckObjectIdPipe) id: string,
  ): Promise<Partial<ProductModel>> {
    return this.productService.product({
      where: { id },
      include: { category: true, brand: true },
    });
  }

  //mutation
  @UseGuards(AuthGuard)
  @requiredRoles(Role.USER)
  @Post()
  async Create(
    @CurrentUser('domainId') domainId: string,
    @CurrentUser('id') createdBy: string,
    @Body() body: CreateProductDto,
  ): Promise<Partial<ProductModel>> {
    const { catId, brandId, ...data } = body;

    return this.productService.create({
      data: {
        ...data,
        domain: { connect: { id: domainId } },
        category: { connect: { id: catId } },
        brand: { connect: { id: brandId } },
        createdBy: { connect: { id: createdBy } },
        updatedBy: { connect: { id: createdBy } },
      },
      include: { category: true, brand: true },
    });
  }

  @UseGuards(AuthGuard)
  @requiredRoles(Role.USER)
  @Put('/:id')
  async Update(
    @CurrentUser('domainId') domainId: string,
    @CurrentUser('id') updatedBy: string,
    @Param('id', CheckObjectIdPipe) id: string,
    @Body() body: UpdateProductDto,
  ): Promise<Partial<ProductModel>> {
    const { brandId, ...data } = body;

    if (!brandId)
      return this.productService.update({
        where: { id, domainId },
        data: {
          ...data,
          updatedBy: { connect: { id: updatedBy } },
        },
        include: { category: true, brand: true },
      });

    return this.productService.update({
      where: { id, domainId },
      data: {
        ...data,
        brand: { connect: { id: brandId } },
        updatedBy: { connect: { id: updatedBy } },
      },
      include: { category: true, brand: true },
    });
  }

  @UseGuards(AuthGuard)
  @requiredRoles(Role.USER)
  @Delete('/:id')
  async Delete(
    @Param('id', CheckObjectIdPipe) id: string,
    @CurrentUser('domainId') domainId: string,
  ): Promise<Partial<ProductModel>> {
    return this.productService.delete({
      where: { id, domainId },
    });
  }
}
