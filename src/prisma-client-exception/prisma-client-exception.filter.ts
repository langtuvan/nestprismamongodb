import { FastifyError } from 'fastify';
import { AbstractHttpAdapter } from '@nestjs/core';

import { Catch, ArgumentsHost, ExceptionFilter } from '@nestjs/common';
import { Prisma } from '@prisma/client';


@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  constructor(private readonly httpAdapterHost: AbstractHttpAdapter) {}
  catch(exception: FastifyError | any, host: ArgumentsHost): void {
    let errorMessage: unknown;
    let httpStatus: number;
    let errorName: string;
    const httpAdapter = this.httpAdapterHost;
    const ctx = host.switchToHttp();

    const message = exception.message.replace(/\n/g, '');

    const code = exception.code;

    if (
      exception instanceof Prisma.PrismaClientKnownRequestError ||
      exception instanceof Prisma.PrismaClientRustPanicError ||
      exception instanceof Prisma.PrismaClientValidationError ||
      exception instanceof Prisma.PrismaClientUnknownRequestError ||
      exception instanceof Prisma.PrismaClientInitializationError
    ) {
      httpStatus = 400;
      errorMessage = message;
      errorName = exception.name + ' - ' + code;

    } else if (
      // useGlobalPipes validate data
      exception.response.statusCode &&
      exception.response.statusCode >= 400 &&
      exception.response.statusCode <= 499
    ) {
      httpStatus = exception.response.statusCode;
      errorMessage = exception.response.message;
      errorName = exception.response.error;
    } else {
      httpStatus = 500;
      errorMessage =
        'Sorry! something went to wrong on our end, Please try again later';
    }
    const errorResponse = {
      statusCode: httpStatus,
      message: errorMessage,
      error: errorName,
    };
    httpAdapter.reply(ctx.getResponse(), errorResponse, httpStatus);
  }
}
