import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UserService } from 'src/router/User/user.service';
import { MenuService } from 'src/router/Menu/menu.services';
import { StaffService } from 'src/router/Staff/staff.service';

@Module({
  controllers: [AuthController],
  providers: [
    PrismaService,
    AuthService,
    UserService,
    StaffService,
    MenuService,
  ],
})
export class AuthModule {}
