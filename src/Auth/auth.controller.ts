
import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Session,
  NotFoundException,
  UnauthorizedException,
  BadRequestException,
  UseGuards,
  Res,
  Param,
  HttpCode,
} from '@nestjs/common';
import { UserService } from 'src/router/User/user.service';
import { Throttle } from '@nestjs/throttler';
import { user as authModel } from '@prisma/client';
import { Response } from 'express';

import {
  ChangePasswordAuthDto,
  SignInAuthDto,
  SignUpAuthDto,
  UpdateCurrentUserDto,
} from 'src/dto/auth';
import { CurrentUser } from 'src/decorators/user.decorator';
import { AuthGuard } from 'src/guard/auth.guard';
import { Role } from 'src/entries/role.enum';
import { MenuService } from 'src/router/Menu/menu.services';
import { MenuType } from 'src/types/menu';
import { CheckObjectIdPipe } from 'src/pipe/validate-objectid.pipe';
import { CurrentDomain } from 'src/decorators/domain.decorator';
import { StaffService } from 'src/router/Staff/staff.service';

type MeModdel = {
  user: Partial<authModel>;
  menu: MenuType;
};

@Controller()
@Throttle({ default: { limit: 10, ttl: 300000 } })
export class AuthController {
  constructor(
    private readonly userService: UserService,
    private readonly menuService: MenuService,
    private readonly staffService: StaffService,
  ) {}

  @Get('me')
  @Throttle({ default: { limit: 100, ttl: 300000 } })
  @UseGuards(AuthGuard)
  async me(@Session() session?: Record<string, any>): Promise<MeModdel> {
    // session-based authentication
    //session.visits = session.visits ? session.visits + 1 : 1;
    // token-based authentication

    return {
      user: this.setUserValue(session.user),
      menu: session.menu,
    };
  }

  @Post('/staff/signin')
  async staff(
    @Body() signInData: SignInAuthDto,
    @Session() session: Record<string, any>,
    @Res() res: Response,
  ) {
    // find user
    if (session.user) {
      session.destroy(function (err) {});
    }

    const staff = await this.staffService.staff({
      where: { email: signInData.email },
    });

    const isMatchPassword = await this.staffService.comparePassword(
      signInData.password,
      staff.password,
    );

    if (!isMatchPassword) {
      throw new UnauthorizedException(`Email or Password maybe wrong.`);
    }

    session.user = this.setUserValue(staff as any);
    session.menu = await this.menuService.menus();
    return res.status(200).json({
      user: session.user,
      menu: session.menu,
    });
  }

  @Post('signin')
  async signinUser(
    @Body() signInData: SignInAuthDto,
    @Session() session: Record<string, any>,
    @CurrentDomain() domainId: string,
    @Res() res: Response,
  ): Promise<Response<MeModdel>> {
    // find user
    if (session.user) {
      session.destroy(function (err) {});
      //throw new BadRequestException(`has already Sign In session !`);
    }

    const user = await this.userService.user({
      where: { email: signInData.email, domainId },
    });

    if (!user) {
      throw new BadRequestException('Email or Password maybe wrong !');
    }

    const isMatchPassword = await this.userService.comparePassword(
      signInData.password,
      user.password,
    );

    if (!isMatchPassword || user.deleted) {
      throw new UnauthorizedException(`Email or Password maybe wrong.`);
    }

    session.user = this.setUserValue(user as authModel);
    session.menu = await this.menuService.menus();
    return res.status(200).json({
      user: session.user,
      menu: session.menu,
    });
  }

  @Post('signout')
  @UseGuards(AuthGuard)
  signOut(@Session() session: Record<string, any>) {
    session.destroy(function (err) {});
    return { status: true };
  }

  @Post('/signup/:domainId')
  async signupUser(
    @Body() signUpInput: SignUpAuthDto,
    @Param('domainId', CheckObjectIdPipe) id: string,
    @CurrentDomain(CheckObjectIdPipe) domainId: string,
    @Res() res: Response,
  ): Promise<authModel | any> {
    const { email, fullname, phone, password, confirmPassword } = signUpInput;

    if (password !== confirmPassword) {
      return res.status(400).json({
        statusCode: 400,
        message: [
          { field: 'confirmPassword', message: 'confirmPassword not match' },
        ],
        error: 'Bad Request',
      });
    }

    await this.userService.checkUserExitting({
      where: { id: domainId },
      userWhereInput: {
        email,
        phone,
      },
    });

    return this.userService
      .create({
        data: {
          email,
          fullname,
          phone,
          //role: [Role.GUEST],
          password: await this.userService.CreatePasswordHash(password),
          domain: { connect: { id: domainId } },
        },
      })
      .then(() => {
        return res.status(200).json({
          statusCode: 200,
          message: true,
        });
      });
  }

  @Put('changepassword')
  @UseGuards(AuthGuard)
  async ChangePassword(
    @Body() dataInput: ChangePasswordAuthDto,
    @CurrentUser('domainId') domainId: string,
    @Session() session: Record<string, any>,
  ): Promise<authModel> {
    const { id } = session.user;
    const { oldPassword, newPassword, confirmPassword } = dataInput;

    if (newPassword !== confirmPassword) {
      throw new BadRequestException({
        message: [
          {
            field: 'confirmPassword',
            message: 'confirmPassword is not match password !',
          },
        ],
      });
    }

    if (oldPassword === newPassword) {
      throw new BadRequestException({
        message: [
          {
            field: 'oldPassword',
            message: 'new password is not equal old password !',
          },
        ],
      });
    }

    const findUser = await this.userService.user({ where: { id, domainId } });

    if (!findUser) {
      throw new NotFoundException(`Id does not exist.`);
    }

    const isMatchPassword = await this.userService.comparePassword(
      oldPassword,
      findUser.password,
    );

    if (!isMatchPassword) {
      throw new BadRequestException({
        message: [
          { field: 'oldPassword', message: 'old password is not match !' },
        ],
      });
    }

    return this.userService.update({
      where: { id },
      data: {
        password: await this.userService.CreatePasswordHash(
          dataInput.newPassword,
        ),
      },
    });
  }

  @Put('updatecurrentuser')
  @UseGuards(AuthGuard)
  async UpdateUser(
    @Res() res: Response,
    @Body() updateCurrentUserData: UpdateCurrentUserDto,
    @CurrentUser('id') id: string,
    @CurrentUser('domainId') domainId: string,
    @Session() session?: Record<string, any>,
  ): Promise<authModel | any> {
    const { fullname, birthday, phone } = updateCurrentUserData;

    await this.userService.checkUserExitting({
      where: { id: domainId },
      userWhereInput: {
        phone,
      },
    });

    const user = await this.userService.update({
      where: { id },
      data: { fullname, birthday, phone },
    });

    session.user = this.setUserValue(user as authModel);
    session.menu = await this.menuService.menus();
    return res.status(200).json({ user: session.user, menu: session.menu });
  }

  setUserValue(user: authModel): Partial<authModel> {
    const {
      id,
      fullname,
      email,
      birthday,
      active,
      role,
      permissions,
      phone,
      comfirmEmail,
      domainId,
      isAdmin,
      isStaff,
    } = user;
    return {
      id,
      fullname,
      email,
      birthday,
      active,
      role,
      permissions,
      phone,
      comfirmEmail,
      domainId,
      isAdmin,
      isStaff,
    };
  }
}
