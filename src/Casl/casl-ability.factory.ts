import {
  PureAbility as Ability,
  AbilityBuilder,
  AbilityClass,
  ExtractSubjectType,
  InferSubjects,
} from '@casl/ability';
import { Injectable } from '@nestjs/common';
import Action from 'src/entries/permission/action.enum';
import { Role } from 'src/entries/role.enum';

export class User {
  id: string;
  role: string[];
  permissions: string[];
}

export class Article {
  id: string;
  isPublished: boolean;
  authorId: string;
}

export class Category {
  id: string;
  authorId: string;
}

type Subjects =
  | InferSubjects<typeof User | typeof Article | typeof Category>
  | 'all';

export type AppAbility = Ability<[Action, Subjects]>;

@Injectable()
export class CaslAbilityFactory {
  defineAbility(user: User) {
    const { can, cannot, build } = new AbilityBuilder<
      Ability<[Action, Subjects]>
    >(Ability as AbilityClass<AppAbility>);

    const { id: userId, role: userRoles, permissions: userPermissions } = user;

    const isAdmin = userRoles.includes(Role.ADMIN);
    const isUser = userRoles.includes(Role.USER);

    if (isAdmin) {
      can(Action.Manage, 'all'); // read-write access to everything
    }

    if (isUser) {
      can(Action.Read, 'all');
      cannot(Action.Delete, User);
    }

    can(Action.Update, User, { id: userId });

    return build({
      // Read https://casl.js.org/v5/en/guide/subject-type-detection#use-classes-as-subject-types for details
      detectSubjectType: (item) =>
        item.constructor as ExtractSubjectType<Subjects>,
    });
  }
}
