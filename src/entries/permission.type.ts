import PostsPermission from './permission/postsPermission.enum';
import CategoriesPermission from './permission/categoriesPermission.enum';
import Action from './permission/action.enum';
import User from './permission/user.enum';

const Permission = {
  ...PostsPermission,
  ...CategoriesPermission,
  ...Action,
  ...User,
};

type Permission = PostsPermission | CategoriesPermission | Action | User;

export default Permission;
