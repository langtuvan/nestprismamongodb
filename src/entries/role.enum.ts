export enum Role {
  ADMIN = 'admin',
  MANAGER = 'manager',
  USER = 'user',
  ACCOUNTANT = 'Accountant',
  SHIPPER = 'shipper',
  SALE = 'sale',
}

export enum StaffRole {
  SUPPERADMIN = 'supperAdmin',
  ADMIN = 'admin',
  MANAGER = 'manager',
  USER = 'user',
  ACCOUNTANT = 'Accountant',
  SHIPPER = 'shipper',
  SALE = 'sale',
}
