enum User {
  ManageUser = 'manageUser',
  CreateUser = 'createUser',
  ReadUser = 'readUser',
  UpdateUser = 'updateUser',
  DeleteUser = 'deleteUser',
}

export default User;
