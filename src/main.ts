import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { PrismaService } from './prisma.service';
import { ValidationPipe, BadRequestException } from '@nestjs/common';
import * as session from 'express-session';
import helmet from 'helmet';
import { readFileSync } from 'fs-extra';
import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';
import { HttpExceptionFilter } from './prisma-client-exception/prisma-client-exception.filter';

const allowedOrigins = [
  'https://nuochoa.webnextapp.com',
  'http://localhost:3000',
];

const corsOptions: CorsOptions = {
  origin: true, //allowedOrigins, // true to active cors
  preflightContinue: false,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
};

const httpsOptions = {
  ca: readFileSync('./secrets/ca_bundle.crt'),
  key: readFileSync('./secrets/private.key'),
  cert: readFileSync('./secrets/certificate.crt'),
};

async function bootstraphttps() {
  const app = await NestFactory.create(AppModule, {
    httpsOptions, // turn on https
    logger: ['error', 'warn'],
  });
  //cors;
  app.enableCors({
    ...corsOptions,
  });

  // helmet
  app.use(
    helmet({
      crossOriginResourcePolicy: false,
    }),
  );

  // session
  app.use(
    session({
      secret: process.env.SECRET_KEY,
      resave: false, // reset session when expire
      saveUninitialized: true,
      cookie: {
        secure: true,
        httpOnly: false,
        sameSite: 'strict', // https setting
        maxAge: Date.now() + 60 * 60 * 24 * 1,
      },
    }),
  );

  // validate Pipe
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors) => {
        const result = errors.map((error) => ({
          field: error.property,
          message: error.constraints[Object.keys(error.constraints)[0]],
        }));
        return new BadRequestException(result);
      },
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
      stopAtFirstError: true,
    }),
  );
  //exception-filter
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new HttpExceptionFilter(httpAdapter));

  app.get(PrismaService);
  app.enableShutdownHooks();

  await app.listen(process.env.PORT);
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    // httpsOptions, // turn on https
    logger: ['error', 'warn'],
  });
  //cors;
  app.enableCors({
    ...corsOptions,
  });

  // helmet
  app.use(
    helmet({
      crossOriginResourcePolicy: false,
    }),
  );

  // session
  app.use(
    session({
      secret: process.env.SECRET_KEY,
      resave: false, // reset session when expire
      saveUninitialized: true,
      cookie: {
        secure: false,
        httpOnly: true,
        sameSite: 'none', // https setting
        maxAge: Date.now() + 6000 * 60 * 24 * 7,
      },
    }),
  );

  // validate Pipe
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors) => {
        const result = errors.map((error) => ({
          field: error.property,
          message: error.constraints[Object.keys(error.constraints)[0]],
        }));
        return new BadRequestException(result);
      },
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
      stopAtFirstError: true,
    }),
  );
  //exception-filter
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new HttpExceptionFilter(httpAdapter));
  app.get(PrismaService);
  app.enableShutdownHooks();
  await app.listen(process.env.PORT);
}

//bootstraphttps();
bootstrap();
