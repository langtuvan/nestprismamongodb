import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';
import ObjectID from 'bson-objectid';

@Injectable()
export class CheckObjectIdPipe implements PipeTransform {
  transform(value: string): string {
    if (!ObjectID.isValid(value)) {
      throw new BadRequestException('ID is not valid !');
    }
    return value;
  }
}
