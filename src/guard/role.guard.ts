import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Role } from '../entries/role.enum';
import { CHECK_ROLES_KEY } from 'src/decorators/roles.decorator';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}
  canActivate(context: ExecutionContext): boolean {
    // 1. get input requiredRoles
    const requiredRoles =
      this.reflector.getAllAndOverride<Role[]>(CHECK_ROLES_KEY, [
        context.getHandler(),
        context.getClass(),
      ]) || [];

    if (requiredRoles.length === 0) {
      return true;
    }
    // 2. Get request session role get session user Roles
    const request = context.switchToHttp().getRequest();
    const sessionRole = request.session.user.role || [];

    // 3. validate
    if (!sessionRole) {
      return false;
    }

    if (sessionRole.includes(Role.ADMIN)) {
      return true;
    }


    const hasAccessRole = requiredRoles.some((roles) =>
    sessionRole.includes(roles),
    );

    if (!hasAccessRole) {
      throw new UnauthorizedException('Role Access Denied !');
    }
    return hasAccessRole;
  }
}
