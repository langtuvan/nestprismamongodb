import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { StaffRole } from '../entries/role.enum';
import { CHECK_STAFF_ROLES_KEY } from 'src/decorators/roles.decorator';
import { staff } from '@prisma/client';

@Injectable()
export class StaffRolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}
  canActivate(context: ExecutionContext): boolean {
    // 1. get input requiredRoles
    const requiredRoles =
      this.reflector.getAllAndOverride<StaffRole[]>(CHECK_STAFF_ROLES_KEY, [
        context.getHandler(),
        context.getClass(),
      ]) || [];

    if (requiredRoles.length === 0) {
      return true;
    }
    // 2. Get request session role get session user Roles
    const request = context.switchToHttp().getRequest();
    const sessionUser = request.session.user;

    const { role = [], isStaff } = sessionUser as staff;

    // 3. validate
    if (!role) {
      return false;
    }

    if (isStaff && role.includes(StaffRole.SUPPERADMIN)) {
      return true;
    }

    const hasAccessRole = requiredRoles.some((roles) => role.includes(roles));

    if (!hasAccessRole) {
      throw new UnauthorizedException('Role Staff Access Denied !');
    }
    return hasAccessRole;
  }
}
