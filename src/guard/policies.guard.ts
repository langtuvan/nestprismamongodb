import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AppAbility, CaslAbilityFactory } from 'src/Casl/casl-ability.factory';
import { PolicyHandler } from 'src/Casl/PolicyHandler';
import { CHECK_POLICIES_KEY } from 'src/decorators/policies.decorator';
import PermissionAction from 'src/entries/permission.type';
import { Role } from 'src/entries/role.enum';

@Injectable()
export class PoliciesGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private caslAbilityFactory: CaslAbilityFactory,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    // 1. get input requiredRoles
    const policyHandlers = this.reflector.get<PolicyHandler[]>(
      CHECK_POLICIES_KEY,
      context.getHandler(),
    );
    // 2. Get request session role , permission
    const request = context.switchToHttp().getRequest();
    const userSession = request.session.user; 
    const user = {
      id: userSession.id,
      role: userSession.role,
      permissions: userSession.permissions,
    };


    const ability = this.caslAbilityFactory.defineAbility(user);

    const hasAccessPermission = policyHandlers.every((handler) =>
      this.execPolicyHandler(handler, ability),
    );

    if (!hasAccessPermission) {
      throw new UnauthorizedException('Permission Access Denied');
    }
    return hasAccessPermission;
  }

  private execPolicyHandler(
    handler: PolicyHandler,
    ability: AppAbility,
  ): boolean {
    if (typeof handler === 'function') {
      return handler(ability);
    }
    return handler.handle(ability);
  }
}
