export type Errors = {
  statusCode: number;
  message: ErrorMessage[];
  error: string;
};

export type ErrorsProps = {
  data: Errors;
  statusCode: number;
};

export type ErrorMessage = { field: string; message: string };
