export type OverviewNavigation = {
  displayNameEN: string;
  displayNameVi: string;
  href: string;
  icon: string;
  children?: { title: string; href: string; }[]
};

export type OverviewMenu = {
  [key: string]: OverviewNavigation
}

export type ManagementNavigation = {
  displayNameVi: string;
  displayNameEN: string;
  href: string;
  icon: string;
  children: {
    displayNameEN: string;
    displayNameVi: string;
    href: string;
  }[];
};

export type MenuType = {
  overviewNavigation: OverviewMenu;
  managementNavigation: ManagementNavigation[];
};
