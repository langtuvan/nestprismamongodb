import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const CurrentDomain = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request: any = ctx.switchToHttp().getRequest();
    return request.headers.domainkey;
  },
);
