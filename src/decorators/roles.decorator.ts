import { SetMetadata } from '@nestjs/common';
import { Role, StaffRole } from '../entries/role.enum';

export const CHECK_ROLES_KEY = 'Roles';
export const requiredRoles = (...Role: Role[]) =>
  SetMetadata(CHECK_ROLES_KEY, Role);

export const CHECK_STAFF_ROLES_KEY = 'StaffRoles';
export const staffRequiredRoles = (...StaffRole: StaffRole[]) =>
  SetMetadata(CHECK_STAFF_ROLES_KEY, StaffRole);
